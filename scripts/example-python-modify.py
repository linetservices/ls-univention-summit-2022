#!/usr/bin/python3

from univention.udm import UDM
from univention.config_registry.backend import ConfigRegistry
from pprint import pprint

ucr = ConfigRegistry()
ucr.load()


def main():

    udmUsers = UDM.admin().version(1).get('users/user')
    user = udmUsers.get("uid=tschueler1,cn=schueler,cn=users,ou=LS,dc=linet,dc=summit")

    user.props.description = "Summit 2022"

    user.save()
    
main()
        
