#!/usr/bin/python3

from univention.udm import UDM
from univention.config_registry.backend import ConfigRegistry
import pymysql

ucr = ConfigRegistry()
ucr.load()


def main():

    udmUsers = UDM.admin().version(1).get('users/user')
    users = udmUsers.search(base=ucr['ldap/base'])

    mysqlDatabases = []
    fdb = pymysql.connect('localhost',ucr['inf/mysql/user'],ucr['inf/mysql/pass'],)
    cursor = fdb.cursor()
    cursor.execute("SHOW DATABASES")
    result = cursor.fetchall()
    fdb.close()

    for r in result:
        mysqlDatabases.append(r[0])

    for user in users:
        if user.props.userDB:
            print(user.props.username)
            for db in user.props.userDB:
                db_name = "{}_{}".format(user.props.username,db)
                if db_name in mysqlDatabases:
                    print("  ✓ {}".format(db_name))
                else:
                    print("  ❌{}".format(db_name))

main()
        
