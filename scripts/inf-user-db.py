#!/usr/bin/python3

from univention.udm import UDM
from univention.config_registry.backend import ConfigRegistry
from random import randint
import univention.debug as ud
import os
import pymysql
import string

name = "inf_user_db"
description = "Creates user DBs"
filter = "(objectClass=organizationalPerson)"
attribute = ["univentionFreeAttribute2"]


ucr = ConfigRegistry()
ucr.load()

def write_user_doc(db,password,userhome,username):
    filepath = "{}/DB_{}_Zugangsdaten.txt".format(userhome,db)
    os.system("sudo -u {} touch {}".format(username,filepath))
    os.system("echo 'Zugansdaten für Datenbank' > {}".format(filepath))
    os.system("echo 'Datenbank {}' >> {}".format(db,filepath))
    os.system("echo 'Benutzer: {}' >> {}".format(db,filepath))
    os.system("echo 'Passwort: {}' >> {}".format(password,filepath))

def create_password():
    chars = "{}{}".format(string.ascii_letters,string.digits)
    password = ""
    for i in range(15):
        password = "{}{}".format(password,chars[randint(0,len(chars)-1)])
    return password

def handler(dn, new, old):
    username = new['uid'][0].decode('UTF-8')
    htmldir  = new['homeDirectory'][0].decode('UTF-8') 

    # Create public_html if necessary
    if not os.path.isdir(htmldir):
        os.system("sudo -u {} mkdir {}/public_html".format(username,htmldir))

    # Create new user-database if necessary
    existing_databases = []
    try:
        fdb = pymysql.connect('localhost',ucr['inf/mysql/user'],ucr['inf/mysql/pass'],)
        cursor = fdb.cursor()
        cursor.execute("SHOW DATABASES LIKE '{}%'".format(username))
        result = cursor.fetchall()
        fdb.close()

        for r in result:
            existing_databases.append(r[0])


    except Exception as e:
        ud.debug(ud.LISTENER, ud.ERROR, '  "%s"' % e)


    for db in new['univentionFreeAttribute2']:
        db_name = "{}_{}".format(username,db.decode('UTF-8'))

        if db_name not in existing_databases:
            # Create database
            password = create_password()

            try:
                fdb = pymysql.connect('localhost',ucr['inf/mysql/user'],ucr['inf/mysql/pass'],)
                cursor = fdb.cursor()
                cursor.execute("CREATE DATABASE `{}`".format(db_name))
                fdb.close()

                fdb = pymysql.connect('localhost',ucr['inf/mysql/user'],ucr['inf/mysql/pass'],)
                cursor = fdb.cursor()
                cursor.execute("CREATE USER '{}'@'%' IDENTIFIED BY '{}'".format(db_name,password))
                cursor.execute("GRANT USAGE ON *.* TO '{}'@'%'".format(db_name))
                cursor.execute("ALTER USER '{}'@'%' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0".format(db_name))
                cursor.execute("GRANT ALL PRIVILEGES ON `{0}`.* TO '{0}'@'%'".format(db_name))
                fdb.commit()
                fdb.close()

                write_user_doc(db_name,password,new['homeDirectory'][0].decode('UTF-8'),username)
               

            except Exception as e:
                ud.debug(ud.LISTENER, ud.ERROR, '  "%s"' % e)


