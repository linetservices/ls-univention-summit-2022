#!/usr/bin/python2.7

from univention.udm import UDM
from univention.config_registry.backend import ConfigRegistry
import univention.debug as ud
from univention.udm import UDM
import re

name = "inf_group_db"
description = "Creates user DBs for users in activated groups"
filter = "(objectClass=univentionGroup)"
attribute = ["univentionFreeAttribute1","uniqueMember"]


ucr = ConfigRegistry()
ucr.load

def handler(dn, new, old):
    

    if 'univentionFreeAttribute1' in new:
        if new['univentionFreeAttribute1'][0].decode('UTF-8') == "1":
            udmUsers = UDM.admin().version(1).get('users/user')
            for user in new['uniqueMember']:
                udm_user = udmUsers.get(user.decode('UTF-8'))
                db_name = re.sub('\_','\-',new['cn'][0].decode('UTF-8'))
                udm_user.props.userDB.append(db_name)
                udm_user.save()

