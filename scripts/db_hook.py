#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Univention UCS@school
#
# Copyright 2017-2021 Univention GmbH
#
# https://www.univention.de/
#
# All rights reserved.
#
# The source code of this program is made available
# under the terms of the GNU Affero General Public License version 3
# (GNU AGPL V3) as published by the Free Software Foundation.
#
# Binary versions of this program provided by Univention to you as
# well as other copyrighted, protected or trademarked materials like
# Logos, graphics, fonts, specific documentations and configurations,
# cryptographic keys etc. are subject to a license agreement between
# you and Univention and not subject to the GNU AGPL V3.
#
# In the case you use this program under the terms of the GNU AGPL V3,
# the program is provided in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License with the Debian GNU/Linux or Univention distribution in file
# /usr/share/common-licenses/AGPL-3; if not, see
# <http://www.gnu.org/licenses/>.

from ucsschool.importer.utils.user_pyhook import UserPyHook
from univention.udm import UDM


class MyHook(UserPyHook):
    supports_dry_run = False  # when False (default) whole class will be skipped during dry-run

    priority = {
        "pre_create": 0,  # functions with value None will be skipped
        "post_create": 1,
        "pre_modify": 0,
        "post_modify": 1,
        "pre_move": 0,
        "post_move": 0,
        "pre_remove": 0,
        "post_remove": 0,
    }

    def pre_create(self, user): pass

    def post_create(self, user):
        udmUsers = UDM.admin().version(1).get('users/user')
        udmUser = udmUsers.get(user.dn)
        importedDBs = udmUser.props.importUserDB.split()

        for db in importedDBs:
            udmUser.props.userDB.append(db)
        udmUser.save()

    def pre_modify(self, user): pass

    def post_modify(self, user): 
        udmUsers = UDM.admin().version(1).get('users/user')
        udmUser = udmUsers.get(user.dn)
        importedDBs = udmUser.props.importUserDB.split()

        for db in importedDBs:
            udmUser.props.userDB.append(db)
        udmUser.save()
    def pre_move(self, user): pass

    def post_move(self, user): pass

    def pre_remove(self, user): pass

    def post_remove(self, user): pass
