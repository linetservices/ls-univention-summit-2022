# LS Univention Summit 2022

Script collection of our presentation at the Univention Summit 2002

## Overview

These are the scripts used for demonstration purposes in our Workshop "UCS Automatisierung für Admins" at Univention Summit 2022

## Scripts

  - check-user-dbs.py: 
    This example script checks if all databases for all users are created
  - db_hook.py:
    This is an example for an UCS@School-Import-Hook. The script activates the user database if a database name is given in the user-import.csv
    These script has to be placed unter ''/usr/share/ucs-school-import/pyhooks/''
  - example-python-list.py, example-python-modify.py, example-udm-list.sh, example-udm-modify.sh
    These 4 example scripts show the basic functions of the UDM shell command and the Python UDM API.
  - inf-group-db.py, inf-user-db.py
    These are the listener modules shown in our presentation. The scripts have to be placed under ''/usr/lib/univention-directory-listener/system''

## License

All scripts are published under the MIT license unless otherwise stated in the script
  


